jQuery(document).ready(function(){
	jQuery(function() {
		jQuery('.grid-gallery-<?php echo $id; ?>').magnificPopup({
			delegate: 'a',
			gallery:{enabled:true},
			type: 'image',
			closeOnContentClick: true,
			closeBtnInside: false
		});
	});
});
