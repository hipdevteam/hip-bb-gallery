.fl-node-<?php echo $id; ?> .grid-gallery {
    float: left;
}

.grid-gallery-item{
    width: <?php echo 100 / $settings->columns_per_row; ?>%;
    padding: 0 <?php echo $settings->photo_spacing; ?>px <?php echo $settings->photo_spacing; ?>px 0;
    float: left;
}
@media(max-width: 480px){
    .grid-gallery-item{
        display: block;
        margin: 0 auto;
        width: 100%;
    }
}

.grid-gallery-item a{
    width: 100%;
    display: block;
    position:relative;
		line-height: 0;
}
.grid-gallery-item a:after{
    content:'';
    position: absolute;
    left: 0;
    top:0;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,<?php echo $settings->overlay?>);
    opacity: 0;
    transition: all 0.3s ease;
}
.grid-gallery-item a:hover:after{
    opacity:1;
}
.grid-gallery-item a i{
    font-size: 30px;
    position: absolute;
    left: calc(50% - 15px);
    top: calc(50% - 15px);
    color: #fff;
    opacity: 0;
    z-index: 1;
    transition: all 0.3s;
}
.grid-gallery-item a:hover i{
    opacity:1;
}

.fl-row-content-wrap .fl-module-gallery .fl-node-content {
    margin: 0;
}

.fl-row-content-wrap .fl-node-content .hip-custom-gallery{
    width: 100%;
    line-height: 0;
    display: flex;
    flex-direction: column;
    list-style: none;
    margin: 0;
    align-items: stretch;
    align-content: stretch;
}
.fl-row-content-wrap .fl-node-content .hip-custom-gallery-item {
    position: relative;
    overflow: hidden;
    transition: all 0.5s ease;
}

.fl-row-content-wrap .fl-node-content .hip-custom-gallery-item img {
    width: 100%;
    object-position: 50%;
    object-fit: cover;
}

@media screen and ( min-width: 512px ) {
    .fl-row-content-wrap .fl-node-content .hip-custom-gallery {
        flex-direction: row;
        flex-wrap: wrap;
    }
    .fl-row-content-wrap .fl-node-content .hip-custom-gallery-item {
        flex-basis: 50%;
        width: 50%;
    }
    .fl-row-content-wrap .fl-node-content .hip-custom-gallery-item img {
        width: 100%;
        min-height: 100%;
    }

    @media screen and ( min-width: 768px ) {
        .fl-row-content-wrap .fl-node-content .hip-custom-gallery {
            flex-wrap: nowrap;
        }
        .fl-row-content-wrap .fl-node-content .hip-custom-gallery-item img {
            width: 100%;
            object-position: 50%;
            object-fit: cover;
        }
    }

