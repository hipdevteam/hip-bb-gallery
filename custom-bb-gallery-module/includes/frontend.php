<?php if($settings->layout == 'grid') : ?>

    <?php $count = 0; ?>
    <div class="grid-gallery grid-gallery-<?php echo $id ?>">
        <?php foreach($module->get_photos() as $photo) : ?>
            <?php $count++; ?>

            <div class="grid-gallery-item">
                <?php if($settings->light_box_option == 'on') : ?>
                    <a href="<?php echo $photo->src ?>" class="popup_gallery popup-gallery-<?php echo $count ?>">
                        <img src="<?php echo $photo->src ?>" alt="gallery-image">
                        <i class="<?php echo $settings->hover_icon ?>"></i>
                    </a>
                <?php else: ?>
                    <img src="<?php echo $photo->src ?>" alt="gallery-image">
                <?php endif; ?>
                <?php if($settings->show_captions == 'on') : ?>
                    <p><?php echo $photo->caption; ?></p>
                <?php endif; ?>
            </div>
            <?php echo $count % $settings->columns_per_row == 0 ? '<div class="fl-clear"></div>' : '';?>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <div class="hip-custom-gallery">
        <?php foreach($module->get_photos() as $photo) : ?>
            <div class="hip-custom-gallery-item">
                <img src="<?php echo $photo->src ?>" alt="gallery image">
            </div>
        <?php endforeach; ?>
    </div>
<?php endif ?>
