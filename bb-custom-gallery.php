<?php
/**
 * Plugin Name: Custom BB Gallery
 * Description: Gallery module for the Beaver Builder Plugin.
 * Version: 0.1.3
 * Author: Hip Creative
 */

define( 'CG_MODULES_DIR', plugin_dir_path( __FILE__ ) );
define( 'CG_MODULES_URL', plugins_url( '/', __FILE__ ) );

function load_custom_bb_gallery() {
    if ( class_exists( 'FLBuilder' ) ) {
        require_once 'custom-bb-gallery-module/custom-bb-gallery-module.php';
    }
}
add_action( 'init', 'load_custom_bb_gallery' );
